# Citotron: Wikipedia Citation Tools

## General aims

Written for the WikiScience research project at the Universitat Oberta de Catalunya, Citotron analyses scholarly citations in Wikipedia.  In the longer term it may become a general toolbox for working with citations on Wikipedia.  Particularly interesting would be to use these functions for automatic citation correction and cohesion in a Wikipedia bot.

## Quickstart

Count the frequency of titles whose ISBN, pmid, pmc or arxiv id is given in small.csv. Sleep one second before requests but use eight concurrent threads. You normally want twice as many threads as the number of CPU cores on the machine. After installation you can do:

`./citotron -m count -i small.csv -s 1 -j 8`

On a machine with four CPU cores it should take around five minutes to query one thousand titles and count them.

## Current operation

Citotron can do a couple of things at the moment:

## Count

`-m types`

Count 

Count the frequency of references to single journal or book titles in a CSV file such as the one in the data directory.  Miscellaneous modes available for further data analysis.  Uses free APIs to resolve references.

## Install

Instructions are for Debian derivative operating systems.

1. Install dependencies:

    sudo apt install python3-pip python3-bs4 python3-joblib python3-requests python3-isbnlib python3-matplotlib
    
    pip3 install those dependencies missing from the repos - e.g. sudo pip3 install -U joblib isbnlib

2. Clone `citotron` repository

    git clone

## Configuration

Citotron has command line arguments for end users and a settings file for developers.

### Command line arguments

Output of `./citotron.py -h`:

    usage: citotron.py [-h] [-m MODE] [-k KIND] [-i INPUTFILE] [-c COMPAREFILE]
                       [-o OUTPUTFILE] [-s SLEEP] [-t TIMEOUT] [-j JOBS] [-v]
    
    optional arguments:
      -h, --help            show this help message and exit
      -m MODE, --mode MODE  Action to perform on data. 'count' counts the
                            frequency of publication (book, journal, etc.) titles
                            in the dataset. 'resolve' only resolves identifiers to
                            titles (used for testing). 'types' counts the
                            frequency of the identifiers themselves in the dataset
                            (used for understanding the dataset).
      -k KIND, --kind KIND  Kind of identifier to process. Default is 'all'.
                            Current choices are 'isbn', 'pmid', 'pmd', 'doi',
                            'arxiv'.
      -i INPUTFILE, --inputfile INPUTFILE
                            Input filename. Default is 'input.csv'.
      -c COMPAREFILE, --comparefile COMPAREFILE
                            Compare with filename. Default is 'other.csv'.
      -o OUTPUTFILE, --outputfile OUTPUTFILE
                            Output filename. Default is 'output.csv'.
      -s SLEEP, --sleep SLEEP
                            Sleep between requests. Default is 2.
      -t TIMEOUT, --timeout TIMEOUT
                            Time to wait for response from server. Default is 6.
      -j JOBS, --jobs JOBS  Number of parallel requests. Default is 1.
      -v, --verbose         Verbose mode. Default is True.
    
### Settings file

Typical users don’t have to edit this file.

Settings are stored in `settings.py`:

    # Citotron settings

    ## General
    
    DATADIR = "data"
    LOGFILE = "log.csv"
    SCIENTIFIC_PUBLISHERS = "scientific_publishers.lst"
    JOURNAL_ABBREVS = "journal_abbrevs.json"
    PMID_JOURNAL_ABBREVS = "pmid_journal_abbrevs.json"
    
    ## Resolver urls
    
    doi0 = "http://su8bj7jh4j.search.serialssolutions.com/?id=DOI"
    
    doi1 = "http://search.crossref.org/dois?q=DOI"
    
    isbn0 = "http://www.amazon.com/gp/search/ref=sr_adv_b/?search-alias=stripbooks&unfiltered=1&field-isbn=ISBN&field-dateop=During&sort=relevanceexprank&Adv-Srch-Books-Submit.x=22&Adv-Srch-Books-Submit.y=5"
    
    isbn1 = "https://www.worldcat.org/search?q=bn%3AISBN&qt=advanced&dblist=638"
    
    isbn2 = "http://www.bookfinder.com/search/?isbn=ISBN&st=xl&ac=qr"
    
    isbn3 = "http://www.isbnsearch.org/isbn/ISBN"
    
    isbn4 = "http://openlibrary.org/api/books?bibkeys=ISBN:ISBN&details=true"
    
    # For checking if an ISBN is "scientific" or not
    isbn10 = "http://wokinfo.com/cgi-bin/bkci/search.cgi"
    
    pmid0 = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pubmed&id=PMID"
    
    pmc0 = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi?db=pmc&id=PMC"
    
    arxiv0 = "http://arxiv.org/abs/ARXIV"
    
    arxiv1 = "http://export.arxiv.org/api/query?id_list=ARXIV"
    

## Issues

Issues, bugs, comments are tracked in the Gitlab [issue tracker](https://gitlab.com/maxigas/citotron/issues).

## Screenshot

Running citotron with no sleep and a dozen threads:

![time ./citotron.py -d ../citotron/data -i cites.csv -s 0 -j 12 count](screenshot0.png)

(Right click on the image and choose “Open image…” to enlarge.)







