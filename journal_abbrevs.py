#!/usr/bin/python3
# Generate ISO 4 List of Title Word Abbreviations (LTWA).
# http://www.issn.org/services/online-services/access-to-the-ltwa/
# Based on the files published with this project:
# http://www.compholio.com/latex/jabbrv/
# Outputs two files:
# 1. journal_abbrevs.json - Journal Title Abbreviations
# 2. journal_partial_abbrevs.json - Partial Journal Title Abbreviations
# Collates all available files independent of language
# Usage:
#     wget http://www.compholio.com/latex/jabbrv/downloads/jabbrv_2014-01-21.tar.gz
#     tar xfz jabbrv_2014-01-21.tar.gz
#     cd jabbrv
#     ./abbrev.py
import latexcodec
from json import dump
inputfilenames = [
    "data/jabbrv/jabbrv-ltwa-all.ldf",
    "data/jabbrv/jabbrv-ltwa-de.ldf",
    "data/jabbrv/jabbrv-ltwa-en.ldf",
    "data/jabbrv/jabbrv-ltwa-es.ldf",
    "data/jabbrv/jabbrv-ltwa-fr.ldf",
]
lines, journal_abbrevs, journal_partial_abbrevs = [], [], []
for inputfilename in inputfilenames:
    with open(inputfilename, mode='rb') as f:
        lines += [l.decode('latex').strip() for l in f.readlines()]
for l in lines:
    if ("%%" in l) or not l:
        continue
    parts = [p.rstrip("}") for p in l.split("{")]
    print('First part:', parts[0])
    print('Second part:', parts[1])
    print('Third part:', parts[2])
    if "JournalAbbreviation" in parts[0]:
        journal_abbrevs.append((parts[2], parts[1]))
    elif "JournalPartialAbbreviation" in parts[0]:
        journal_partial_abbrevs.append((parts[2], parts[1]))
    else:
        pass
#print(journal_abbrevs)
journal_abbrevs = dict(journal_abbrevs)
journal_partial_abbrevs = dict(journal_partial_abbrevs)
with open('data/journal_abbrevs.json', mode='w', encoding='utf-8') as f:
    dump(journal_abbrevs, f, indent=0, ensure_ascii=False)
with open('data/journal_partial_abbrevs.json', mode='w', encoding='utf-8') as f:
    dump(journal_partial_abbrevs, f, indent=0, ensure_ascii=False)
#print(journal_abbrevs)
print("Output saved in data/ to 'journal_abbrevs.json' and 'journal_partial_abbrevs.json'.")

